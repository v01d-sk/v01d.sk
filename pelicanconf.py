#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'V01D'
SITENAME = u'V01D hackerspace'
#SITESUBTITLE = u'Blah blah'
SHOW_SITESUBTITLE_IN_HTML = True
SITEURL = '.'

PATH = 'content'


STATIC_PATHS  = ['posts', 'pages', '.']
ARTICLE_PATHS = ['posts']
OUTPUT_PATH = 'public'
THEME = 'nice-blog'
THEME_COLOR = 'v01d'
SIDEBAR_DISPLAY = ['about', 'tags']
COLOR_SCHEME_CSS = 'github.css'
SIDEBAR_ABOUT = """
<p>
Vitajte na stránkach košického <a href="https://wiki.hackerspaces.org/">hackerspace</a> V01D.

Hackerspaces sú miesta, kde ľudia môžu premýšľať, skúmať a diskutovať o technológiách a digitálnom
umení a spolupracovať na svojich projektoch.
</p>

<p>
V prípade záujmu nás môžete <a href="./pages/kontakt.html">navštíviť, kontaktovať, alebo finančne podporiť</a>.
</p>
"""
DISPLAY_PAGES_ON_MENU = True
DISPLAY_CATEGORIES_ON_MENU = False

TIMEZONE = 'Europe/Bratislava'
DEFAULT_DATE_FORMAT = '%Y-%m-%d'

DEFAULT_LANG = u'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None


# TODO?
# PLUGIN_PATHS = ['pelican-plugins']
# PLUGINS = ['neighbors']

# Social widget
SOCIAL = (('github',     'https://github.com/V01D-hacKErspace',        'GitHub'),
          ('gitlab',     'https://gitlab.com/v01d-sk/',                'GitLab'),
          ('envelope',   'mailto:v01d@v01d.sk',                        'Mail'),
          ('envelope-o', 'https://lists.v01d.sk/',                     'Mailing List'),
          ('comments',   'http://webchat.freenode.net/?channels=v01d', 'IRC'))

DEFAULT_PAGINATION = None

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
