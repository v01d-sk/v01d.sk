#!/usr/bin/python
# -*- coding: utf-8 -*-


import csv
from datetime import datetime, timedelta

preamble = """
========
Kalendár
========

:slug: kalendar
"""

class Event:
    def __init__(self, name, date, description):
        self.name        = name
        self.date        = date
        self.description = description

meetup_count = 6
# Add new events here
fixed_events = [ #Event("test", datetime.now(), "test event")
               ]

# TODO: way to cancel meetups or replace/customize them
#       (e.g. when there's a workshop on a meetup)

def get_meetups():
    #start = datetime(2017, 4, 25, 18, 0)
    start = datetime(2019, 9, 23, 18, 0)
    now = datetime.now()
    # this is really lazy
    while now >= start:
        start += timedelta(days=14)
    unfiltered = [Event("V01D meetup (otvorené verejnosti)", start + timedelta(14 * i), "Pravidelné stretnutie členov aj nečlenov V01D") for i in range(meetup_count)]
    # hack for february 2019 moving
    # return [x for x in unfiltered if (x.date.year != 2019 or x.date.month != 2)]
    return unfiltered

def get_events(filename):
    result = fixed_events + get_meetups()
    now = datetime.now()
    with open(filename, "r") as eventfile:
        eventreader = csv.reader(eventfile, quotechar='"')
        for row in eventreader:
            name = row[0]
            date = datetime.strptime(row[1].strip(), "%Y-%m-%d %H:%M")
            description = row[2]
            if date >= datetime.now():
                result.append(Event(name, date, description))
    return sorted(result, key=lambda event: event.date)

print( "Nothing to do at the moment" )
#with open("content/pages/kalendar.rst", "w") as kalendar:
#    kalendar.write(preamble)
#    kalendar.write(":date: " + datetime.now().strftime("%Y-%m-%d") + "\n\n")
#
#    for event in get_events("events.csv"):
#        datestr = event.date.strftime("%a %Y-%m-%d %H:%M")
#        kalendar.write("* **{}**\n\n  ``{}``\n\n  {}\n\n".format(event.name, datestr, event.description))


