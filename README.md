============
v01d.sk site
============

Static site generated using [Pelican](http://getpelican.com).

Pelican uses [ReStructuredText](http://docutils.sourceforge.net/docs/user/rst/quickref.html) -
a more powerful markdown-like format used mostly by Python projects.

-----------------------------------------
Installing dependencies to build the site
-----------------------------------------

* Install Python 2.x (Debian: `sudo apt install python`)
* Install `pip` (Debian: `sudo apt install python-pip`)
* `pip install pelican`


-------------------------------------
Building the site locally for testing
-------------------------------------

* Generate calendar: `./kalendar.py`
* Generate the site: `pelican -s pelicanconf.py --relative-urls`


------------------
Writing a new post
------------------

* `cd content/posts`
* copy an existing post
* change `date`, `slug`, heading and content

-------------------------------
Adding an event to the calendar
-------------------------------

* Add a line in this format to 'events.csv':

  Test, 2017-04-27 18:00, "Test event, stuff will happen"

  May need to add some code to e.g.  modify a meetup event (these are
  generated in kalendar.py). Pull requests welcome.


-------------------
Directory structure
-------------------

* `content/posts`: Blog posts
* `content/pages`: Site pages (with buttons in nav bar)
* `pelicanconf.py`: Site configuration
* `publishconf.py`: Site configuration changes used for publishing the final
   site but not for local testing
* `.gitlab-ci.yml`: Instructions for publishing the site on gitlab pages
* `kalendar.py`: Script used to generate the calendar page.
   Edit to add events, run periodically from cron.
* `nice-blog`: Site theme
* `nice-blog/static/css`: edit CSS here
* `nice-blog/templates`: edit HTML here

