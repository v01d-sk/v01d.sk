==================================
Workshop: Rozširovanie (Neo)Vimu 1
==================================

:date: 2018-01-06 18:15
:category: status
:tags: workshop, v01d, vim

V utorok 30.01.2018 o 18:30 bude ďalší Vim workshop (po niekoľkých odkladoch).

V priestoroch v01d na Národnej Triede 74 (ak sú zavreté dvere, klopať na okno
so Snowdenom).

Zoberieme Neo/Vim a ukážeme si ako si ho prispôsobiť, ako rozšíriť jeho jazyk na
editovanie textu a ako inštalovať pluginy. Bude aj prehľad najužitočnejších 
pluginov predinštalovaných v predpripravenom prostredí.

`Vim <https://vim.org>`_ je textový editor ktorý používajú milióny ľudí
napriek jeho priam `prehistorickému
<https://en.wikipedia.org/wiki/Ed_(text_editor)>`_ pôvodu.  Jeden z dôvodov
popularity Vimu je práve jeho obrovská rozšíriteľnosť; Vim často slúži ako
prostredie na prototypovanie nových spôsobov ako editovať text. `Neovim
<https://neovim.io>`_ je nový projekt snažiaci sa "prečistiť" Vim a urýchliť
jeho vývoj.

Požiadavky:

- notebook
- ssh, git, vim alebo neovim

Znalosť Vimu nie je nutná (ale pomôže).

Prístup voľný.

.. image:: {attach}extending-neovim-workshop.png
   :width: 70%
   :alt: Extending NeoVim
