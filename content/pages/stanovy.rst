=======
Stanovy
=======

:date: 2017-04-24
:slug: stanovy

-------------------------------
Článok I – Základné ustanovenia
-------------------------------

**I.1** Názov občianskeho združenia je: V01D

**I.2** Sídlom združenia je: L. Novomeského 5, 040 01, Košice

**I.3** Webová stránka združenia je: http://v01d.sk

-------------------------------------
Článok II – Ciele a činnosť združenia
-------------------------------------

**II.1** Základným cieľom združenia je podpora rozvoja vedomostí a zručností v oblasti technológií, vedy, umenia a kultúry.

**II.2** Svoju činnosť združenie vykonáva najmä:

a\) organizovaním seminárov v rôznych oblastiach technológií a vedy;

b\) usporadúvaním workshopov a školení;

c\) získavaním, sústredením a zverejňovaním informácií;

d\) vedeckým a patavedeckým výskumom a vývojom v rôznych oblastiach technológií;

e\) expertnou, konzultačnou a publikačnou činnosťou;

f\) usporadúvaním a spoluorganizovaním medzinárodných konferencií, prednášok, stretnutí a diskusií týkajúcich sa oblastí záujmu združenia.

**II.3** Za účelom napĺňania svojich cieľov a výkonu svojej činnosti združenie:

a\) spolupracuje s inými združeniami a organizáciami v rámci Slovenskej republiky i v zahraničí, s ktorými môže uzatvárať zmluvy o spolupráci;

b\) získava finančné prostriedky, ktoré účelne používa na dosiahnutie svojho cieľa.

---------------------
Článok III – Členstvo
---------------------

**III.1** Členmi združenia sú fyzické alebo právnické osoby, ktoré sa aktívne alebo pasívne podieľajú na jeho činnosti.

**III.2** Združenie má nasledujúce kategórie členstva:

a\) zakladajúci členovia;

b\) aktívni členovia;

**III.3** Zakladajúci členovia sú fyzické osoby, ktoré sa podieľali na založení združenia. Zakladajúci členovia združenia sú uvedení v Návrhu na registráciu občianskeho združenia.

**III.4** Aktívni členovia sú fyzické osoby, ktoré sa aktívne podieľajú na činnostiach združenia alebo na jeho chode. O statuse aktívneho člena rozhoduje hlasovaním Zhromaždenie členov na svojej schôdzi. Za aktívnych členov sa automaticky pokladajú aj zakladajúci členovia, pokiaľ nepožiadajú o odobratie aktívneho členstva.

**III.5** Členovia združenia majú právo zúčastňovať sa na aktivitách združenia a byť informovaní o jeho činnosti.

**III.6** Aktívni členovia majú právo zastávať jednotlivé role v rámci združenia, voliť a byť volení do orgánov združenia.

**III.7** Členovia združenia sú povinní dodržiavať jeho stanovy a rozhodnutia jeho orgánov a zostať živí.

**III.8** Status zakladajúceho člena zaniká:

a\) dobrovoľným vzdaním sa členstva k určitému dátumu;

b\) zánikom združenia (k dátumu zániku združenia);

**III.9** Status aktívneho člena zaniká:

a\) dobrovoľným vzdaním sa členstva k určitému dátumu;

b\) vylúčením po hlasovaní Zhromaždenia členov (k dátumu hlasovania);

c\) zánikom združenia (k dátumu zániku združenia);

---------------------------------
Článok IV – Organizačná štruktúra
---------------------------------

**IV.1** Orgánmi združenia sú:

a\) Zhromaždenie členov;

b\) Riadiaci výbor;

c\) Kontrolná komisia.

**IV.2** Svoju činnosť bude združenie vykonávať v rámci:

a\) Pracovných skupín;

b\) Projektových tímov.

------------------------------
Článok V – Zhromaždenie členov
------------------------------

**V.1** Zhromaždenie členov je najvyšším rozhodovacím orgánom združenia, tvoria ho všetci zakladajúci a aktívni členovia združenia. Jeho stretnutie sa organizuje minimálne raz ročne a zvoláva ho Riadiaci výbor.

**V.2** Zhromaždenie členov svoje uznesenia schvaľuje nadpolovičnou väčšinou hlasov všetkých zástupcov, ak stanovy neurčujú inak. Každý zástupca disponuje jedným hlasom a hlasuje osobne.

**V.3** Zhromaždenie členov svojim hlasovaním najmä:

a\) schvaľuje pravidlá fungovania združenia;

b\) spomedzi seba na obdobie jedného roka volí zástupcov Riadiaceho výboru, ktorí prejavia o túto funkciu záujem;

c\) spomedzi seba na obdobie jedného roka volí zástupcov Kontrolnej komisie, ktorí prejavia o túto funkciu záujem;

d\) pri porušení povinností členmi môže hlasovaním vylúčiť členov združenia, odvolať zástupcov Riadiaceho výboru alebo Kontrolnej komisie;

e\) podľa návrhov členov združenia schvaľuje plán činnosti, pracovné skupiny a projekty združenia;

f\) podľa návrhov Riadiaceho výboru schvaľuje rozpočet združenia na obdobie jedného roka;

g\) schvaľuje plán rozvoja a aktivít združenia;

h\) schvaľuje Výročnú správu a Správu Kontrolnej komisie

i\) dvojtretinovou väčšinou všetkých aktívnych členov a so súhlasom všetkých zakladajúcich členov mení stanovy Združenia.

**V.4** Mimoriadne stretnutie Zhromaždenia členov je povinný zvolať Riadiaci výbor do 15 dní od doručenia žiadosti minimálne tretiny aktívnych členov, žiadosť musí obsahovať dôvod, pre ktorý sa mimoriadne stretnutie zvoláva. Ak Riadiaci výbor do 15 dní nezvolá mimoriadne stretnutie Zhromaždenia členov, Zhromaždenie sa zíde šestnásty deň od doručenia žiadosti Riadiacemu výboru o 18:00 v sídle združenia.

--------------------------
Článok VI – Riadiaci výbor
--------------------------

**VI.1** Riadiaci výbor je najvyšším riadiacim orgánom združenia. Každý zástupca Riadiaceho výboru je štatutárnym zástupcom združenia, koná samostatne. V právnych vzťahoch môže Riadiaci výbor zastupovaním združenia na dobu určitú poveriť inú osobu.

**VI.2** Riadiaci výbor má 5 zástupcov, ktorí sú volení na obdobie jedného roka. Aspoň 1 zástupca v rámci Riadiaceho výboru je zakladajúcim členom združenia.

**VI.3** Riadiaci výbor riadi po organizačnej a hospodárskej stránke činnosť združenia, a to najmä:

a\) zodpovedá za správu hospodárskych záležitostí, vykonaním čiastkových úloh súvisiacich s hospodárením združenia;

b\) uzatvára pracovné dohody so spolupracovníkmi združenia a v závislosti od rozpočtu určuje pre nich finančnú odmenu,

c\) zodpovedá za dobré meno združenia a jeho zviditeľňovanie sa, vykonaním čiastkových úloh súvisiacich s propagáciou združenia

d\) zodpovedá za zaistenie potrebných zdrojov na chod združenia a výkon jeho činností,

e\) zodpovedá za priestory a infraštruktúru združenia

f\) menuje a odvoláva Vedúcich pracovných skupín a Vedúcich projektov združenia;

g\) predkladá Zhromaždeniu členov Výročnú správu združenia, ktorej súčasťou je aj Účtovná uzávierka, Správa Kontrolnej komisie a Správy o činnosti združenia, a to raz ročne do 1. marca za obdobie predchádzajúceho kalendárneho roka;

h\) v období medzi stretnutiami Zhromaždenia členov rozhoduje o ďalších otázkach, ktoré nie sú týmito stanovami priznané iným orgánom;

i\) v odôvodnených prípadoch pozastavuje členstvo aktívnym členom do rozhodnutia Zhromaždenia členov.

**VI.4** Mandát zástupcom Riadiaceho výboru zaniká:

a\) dobrovoľným vzdaním sa funkcie k určitému dátumu;

b\) odvolaním po hlasovaní Zhromaždenia členov (k dátumu hlasovania);

c\) zánikom jeho členstva v združení (k dátumu zániku členstva);

d\) zánikom združenia (k dátumu zániku združenia);

------------------------------
Článok VII – Kontrolná komisia
------------------------------

**VII.1** Kontrolná komisia je kontrolným orgánom združenia, kontroluje dodržiavanie stanov, hospodárenie s finančnými prostriedkami a iným majetkom združenia.

**VII.2** Kontrolná komisia má 2 zástupcov, ktorí sú volení na obdobie jedného roka.

**VII.3** O výsledkoch vykonaných kontrol a o svojej činnosti podáva Zhromaždeniu členov správu. Čiastkovú správu podáva do 15 dní od žiadosti Zhromaždenia členov a ročnú správu raz ročne do 1. marca za obdobie predchádzajúceho kalendárneho roka.

**VII.4** Rola zástupcu Kontrolnej komisie je nezlučiteľná s rolou zástupcu Riadiaceho výboru.

**VII.5** Mandát zástupcom Kontrolnej komisie zaniká:

a\) dobrovoľným vzdaním sa funkcie k určitému dátumu;

b\) odvolaním po hlasovaní Zhromaždenia členov (k dátumu hlasovania);

c\) zánikom združenia (k dátumu zániku združenia);

------------------------------
Článok VIII – Pracovné skupiny
------------------------------

**VIII.1** Pracovné skupiny sú trvalé organizačné jednotky združenia.

**VIII.2** Pracovné skupiny a ich činnosť schvaľuje Riadiaci výbor na základe návrhu aspoň dvoch aktívnych členov

**VIII.3** Pracovné skupiny riadia Vedúci pracovných skupín a svoju činnosť si organizujú samostatne.

---------------------------
Článok IX - Projektové tímy
---------------------------

**IX.1** Projektové tímy sú dočasné organizačné jednotky združenia.

**IX.2** Projekty, na ktorých projektové tímy pracujú schvaľuje Riadiaci výbor na základe návrhu aspoň dvoch aktívnych členov.

**IX.3** Projektové tímy riadia Vedúci projektov a svoju činnosť si organizujú samostatne.

------------------------------
Článok X – Zásady hospodárenia
------------------------------

**X.1** Združenie hospodári s hnuteľným a nehnuteľným majetkom podľa schváleného rozpočtu a hospodárne.

**X.2** Majetok a finančné prostriedky združenia musia byť použité v súlade s cieľmi združenia.

**X.3** Zdrojmi majetku združenia sú najmä členské príspevky, dotácie, granty a dary od právnických a fyzických osôb, a výnosy z majetku a vlastnej činnosti.

---------------------------
Článok XI – Zánik združenia
---------------------------

**XI.1** Združenie zaniká jeho zlúčením s iným občianskym združením alebo dobrovoľným rozpustením.

**XI.2** Združenie môže zaniknúť iba so súhlasom 4/5 jeho zakladajúcich členov.

**XI.3** Pri zániku združenia na návrh Riadiaceho výboru rozhoduje Zhromaždenie členov o použití majetku združenia zostávajúceho po vyrovnaní všetkých záväzkov. Likvidačný zostatok môže byť prevedený iba na inú mimovládnu neziskovú organizáciu zaoberajúcu sa obdobnou činnosťou ako združenie.

----------------------------------
Článok XII – Záverečné ustanovenia
----------------------------------

**XII.1** Stanovy združenia môžu byť zmenené iba na základe rozhodnutia Zhromaždenia členov, ak sa pre zmenu vysloví viac ako dve tretiny členov Zhromaždenia členov vrátane všetkých zakladajúcich členov.
