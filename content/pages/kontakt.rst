=======
Kontakt
=======

:date: 2017-04-24
:slug: kontakt

----------------
Virtuálny výskyt
----------------

Vačšina členov v01du je dostihnuteľná na mailing liste. 
IRC, FB sú skoro vždy mŕtve.

* **E-mail**

  Kontaktovať nás môžeš na adrese `v01d@v01d.sk <mailto:v01d@v01d.sk>`_,
  prípadne napísať na náš mailing list `v01d-general-discussion <https://lists.v01d.sk/mailman/listinfo/v01d-general-discussion>`_.

  Zoznam všetkých mailing listov nájdeš na adrese https://lists.v01d.sk/.

* **IRC** (dead)

  Zvykneme okupovať aj IRC kanál `#v01d <http://webchat.freenode.net/?channels=v01d>`_ na `FreeNode <http://freenode.net/>`_.

  Zoznam `serverov FreeNode <http://freenode.net/irc_servers.shtml>`_:

  ========= ======================================================================================
  IPv4 EU   chat.eu.freenode.net
  IPv4 svet chat.freenode.net
  IPv6      ipv6.chat.freenode.net
  Porty     6665, 6666, 6667, 6697 (SSL only), 7000 (SSL only), 7070 (SSL only), 8000, 8001 a 8002
  ========= ======================================================================================

* **Facebook** (dead)

  Na Facebooku nás nájdete na adrese:

  https://www.facebook.com/pages/V01D/214321521976022

----------------
Fyzický priestor
----------------

Možno v nejakej inej branchi reality, čakáme na PR.

---------------
Kontaktné údaje
---------------

| V01D
| L. Novomeského 5
| 040 01 Košice
| IČO: 42321506

**Bankový účet**

* Pre Slovensko: 2800407166 / 8330
* Pre Českú republiku: 2800407166 / 2010

**Paypal**

.. raw:: html

   <form action='https://www.paypal.com/cgi-bin/webscr' method='post'>
   <input type='hidden' name='cmd' value='_s-xclick' />
   <input type='hidden' name='hosted_button_id' value='KY5BZHNAPNJM2' />
   <span class='box' style='font-weight: bold; background-color: black;'>
       <input type='image' name='submit' src='https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif' alt='PayPal' />
   </span>
   </form>
